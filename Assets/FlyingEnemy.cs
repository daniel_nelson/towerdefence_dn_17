﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlyingEnemy : Enemy {

	public Rigidbody body;
	public ParticleSystem smoke;
	// Use this for initialization
	public override void Start () {
		type = "Flying";
		fundsWorth = 100;
		currentHealth = maxHealth;
		base.Start ();
	}
	
	// Update is called once per frame
	protected override void Update () {


        base.Update();
        if (!smoke.gameObject.activeSelf) return;
        if (currentHealth <= maxHealth / 2) {
            //if (!smoke.isPlaying) {
            smoke.Play();
            //}
        } else {
            smoke.Stop();
        }
    }

	
}
