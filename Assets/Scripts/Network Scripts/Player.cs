﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Player : BaseHealth {//BaseWeapon {// : NetworkBehaviour {

    [SyncVar]
    private bool _isDead = false; // this shows if the player is dead
    public bool isDead // this is an accessor 
    {
        get { return _isDead; } //anything can check if the player is dead
        protected set { _isDead = value; } //only player class or those that derive from it can change it
    }


	private float timeToWaitBeforeRegen = 10;
	private float timePassedTillRegen;
	private bool isDamaged = false;

    [SerializeField]
    private int _funds = 200;
    public int funds {
        get { return _funds; }
        set { _funds = value; }
    }

    Camera mainCamera;
    //[SerializeField]
    //private int maxHealth = 100; // i might move this to a base health system later 

    // to mark as sync variable use this
    //[SyncVar] //everytime value changes it will be pushed to all clients :D
    //private int currentHealth; //NEEDs to be synced across all clients 

    private PlayerUI UI;

    [SerializeField]
    private Behaviour[] disableOnDeath;

    private bool[] wasEnabled;


    public void Setup(GameObject playerUIInstance) // player setup is called from the playersetip script
    {
        wasEnabled = new bool[disableOnDeath.Length];
        for (int i = 0; i < wasEnabled.Length; i++) {
            wasEnabled[i] = disableOnDeath[i].enabled;
            //this simply renables all the componentes that were disabled on death
        }

        UI = playerUIInstance.GetComponent<PlayerUI>();
        setDefaults();
    }

    public void setDefaults() {
        currentHealth = maxHealth;
        for (int i = 0; i < disableOnDeath.Length; i++) {
            disableOnDeath[i].enabled = wasEnabled[i];
            // loop through all the componenets and state weather they were enabled orginally
            // need speical case for colliders since they are not behavious.
            // colliders are derived from components which behaviours derived from. 
        }

        //special case to handle the collider 
        Collider col = GetComponent<Collider>();
        if (col != null) {
            col.enabled = true;
        }
    }



    void Update() {
        if (!isLocalPlayer)
            return;
		
		//Health Regen

		if (isDamaged) {
			
			timePassedTillRegen += Time.deltaTime;
			if (timePassedTillRegen > timeToWaitBeforeRegen) {
				if (currentHealth < maxHealth) {
					currentHealth += 1;
				}
			}
		}

		if (currentHealth == maxHealth) {
			isDamaged = false;
			timePassedTillRegen = 0;
		}



        //this is test code to see if I can kill the player properly 
        if (Input.GetKeyDown(KeyCode.K)) {
            TakeDamage(70); //ITS OVER 9000!
        }

    }


    public override void TakeDamage(float _amount) {
        //only take damage if not dead
        if (isDead) {
            return;
        }
        base.TakeDamage(_amount);

		isDamaged = true;
    }

    //private so player can only die through take damage method. 
    protected override void Die() {
        isDead = true;
        print("nooo");
        //Disable components 
        Debug.Log(transform.name + "is dead");

        for (int i = 0; i < disableOnDeath.Length; i++) {
            disableOnDeath[i].enabled = false;
        }

        //special case to handle the collider 
        Collider col = GetComponent<Collider>();
        if (col != null) {
            col.enabled = false;
        }

        //call respawn method
        //Coroutine is executed until an yield instruction is found, its becuase of the time thing (3 seconds)
        StartCoroutine(Respawn());


    }

    //this is the respawn method 
    private IEnumerator Respawn() {
        //to find respawn time, it is stored in the match setting class 
        yield return new WaitForSeconds(GameManager.singleton.matchsettings.respawnTime); // wait for 3 seconds. 
        setDefaults(); //reset all the defaults - will enable components again

        //TODO: check to make sure the network start position is there

        //this will return one of the spawn point registered in the network manager 
        Transform _spawnPoint = NetworkManager.singleton.GetStartPosition();
        transform.position = _spawnPoint.position;
        transform.rotation = _spawnPoint.rotation;

        Debug.Log("Player respawned");

    }


}
