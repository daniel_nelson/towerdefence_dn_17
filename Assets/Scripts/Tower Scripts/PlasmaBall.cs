﻿using UnityEngine;
using System.Collections;

public class PlasmaBall : MonoBehaviour {

    GameObject parent;
    float fireTimer;
	public ParticleSystem flare;
	// Use this for initialization
	void Start () {
        parent = transform.parent.parent.gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        fireTimer = parent.GetComponent<ShootingSystem>().mfireTimer / 1.5f;
        print(fireTimer);
		if (fireTimer > 0) {
			if (!flare.isPlaying) {
				flare.Play ();
			}
		}
        transform.localScale = new Vector3(fireTimer, fireTimer, fireTimer*1.2f);
    }
}
