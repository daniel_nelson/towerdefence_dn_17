﻿using UnityEngine;
using System.Collections;

public class ElectricTower : Tower {

	// Use this for initialization
	void Start (){
	}

	public ElectricTower(string type, GameObject prefab, int price, Vector3 pos) : base(type, prefab, price, pos){
	}

	public ElectricTower(GameObject prefab, int price) :base (prefab, price) {
	}

	public ElectricTower(Tower tower) :base(tower) {
	}

	public ElectricTower (GameObject prefab) : base(prefab){
	}

	// Update is called once per frame
	void Update () {
	}
}
