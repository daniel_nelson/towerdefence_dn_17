﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
[RequireComponent(typeof(WeaponManger))]
public class PlayerShoot : NetworkBehaviour {


    [SerializeField]
    public BaseWeapon currentWeapon; //should this be synced? - no dont think so - R

    [SerializeField]
    private WeaponManger weaponManager;

    //Note: I can make the gun on a seperate layer, then have it rendered by another camera
    //and have that camera go ontop of the current camera
    //what this would do is basicly prevent the gun clipping in walls - dont know if its worth it yet
    //this would also mean I can make the gun look very close - like in FPS games
    //while the model could hold the gun at a more reasonable range

    //convention for tags
    private const string PLAYER_TAG = "Player";

    private const string ENEMY_TAG = "Enemy";

    Ray ray;
    //control what we hit - for example freindly players.
    [SerializeField]
    private LayerMask mask;
    Camera mainCamera;

    HitMarkerScript hitmarker;

    // Use this for initialization
    void Start() {
        hitmarker = GameObject.FindGameObjectWithTag("Hitmarker").GetComponent<HitMarkerScript>();

        //Debug.Log ("The gun is active");
        if (!isLocalPlayer) {
            return;
        }
        Debug.Log("shoot start complete for player");
        mainCamera = FindCamera();
        weaponManager = GetComponent<WeaponManger>();

    }
    private Camera FindCamera() {
        if (GetComponent<Camera>()) {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }
    [Command]
    void CmdDebug(string s) {
        print(s);
    }
    [Command]
    void CmdSpawn(string s) {
        print(s);
        EnemyManager.singleton.CmdSpawnEnemy(Enemy.PLACEHOLDER, new Vector3(0, .5f, 0), 5);
		EnemyManager.singleton.CmdSpawnEnemy(Enemy.FLYING, new Vector3(3, .5f, 0), 5);
		EnemyManager.singleton.CmdSpawnEnemy(Enemy.FASTFLYING, new Vector3(-3, 5f, 0), 5);
    }

    //IMPORTANT: this must be a command as syncvars are only syncing from server TO all clients so you must effect the variable on the server side to make it sync up with the clients
    //at least thats what i figure

    //OLD
    /*[Command]
    void CmdSht(float dmg) {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.forward));
        DrawLine(ray.origin + ray.direction * 2f, ray.origin + ray.direction * 50f, Color.cyan, 5f);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 200,
                    Physics.DefaultRaycastLayers)) {
            if (hit.collider.GetComponent<BaseHealth>()) {
                hit.collider.GetComponent<BaseHealth>().TakeDamage(dmg);
            }
        }
    }
    */

    [Command]
    void CmdShoot(Vector3 _pos, Vector3 _dir, float _dmg, float _range) {
        RaycastHit hit;
        if (Physics.Raycast(_pos, _dir, out hit, _range,
            Physics.DefaultRaycastLayers)) {
            if (hit.collider.GetComponent<BaseHealth>()) {
                hit.collider.GetComponent<BaseHealth>().TakeDamage(_dmg);
            }
        } else Debug.LogError("client shot something which was not in the same place/not spawned in the server");
    }
    public void Shoot() {
        ray = mainCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y, 0));
        RaycastHit hitHit;
        if (Physics.Raycast(ray, out hitHit, currentWeapon.maxRange, Physics.DefaultRaycastLayers)) {
            if (hitHit.collider.gameObject.GetComponent<BaseHealth>()) {
                hitmarker.Hit();
                CmdShoot(ray.origin, ray.direction, currentWeapon.damage, currentWeapon.maxRange);
            }
        }
        
        UpdateShootingStatistics();
    }
    //TODO: make spread work
    public void ShotGunShoot() {
        CmdShoot(ray.origin, ray.direction, currentWeapon.damage / 4, currentWeapon.maxRange);
        CmdShoot(ray.origin, ray.direction, currentWeapon.damage / 4, currentWeapon.maxRange);
        CmdShoot(ray.origin, ray.direction, currentWeapon.damage / 4, currentWeapon.maxRange);
        CmdShoot(ray.origin, ray.direction, currentWeapon.damage / 4, currentWeapon.maxRange);


        UpdateShootingStatistics();
    }
    void UpdateShootingStatistics ()
	{
		Statistics.timeWithoutShots = 0;
		Statistics.shotsFired += 1;
	}

    void Update() {
		if (!isLocalPlayer) {
			//Debug.Log ("No network idenity found or is not the right player.");
			return; //this is stopping th ray cast from being called...
		} else {
			//these two bits of code are temporary as player shoot was buggy and needed to put debug spawn button on player somehwere
			if (Input.GetKeyDown (KeyCode.F)) {
				CmdSpawn ("spawn");
			}
            
			currentWeapon = weaponManager.GetCurrentWeapon ();
			if (currentWeapon != null) {
				//Changed
				//if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0 && !currentWeapon.gunAnim.isPlaying) {
				if (Input.GetButtonDown ("Fire1") && !currentWeapon.gunAnim.isPlaying) {
					Debug.Log("shoot");
					//Shoot ();



				} else {
					//The stats code
					Statistics.timeWithoutShots += Time.deltaTime;
					if (Statistics.timeWithoutShots > 5) {
						Statistics.shotsFired = 0;
					}
				}

			
			}
		}
	}

    
    #region temp
    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f) {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
        lr.SetWidth(0.1f, 0.1f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }
    #endregion
}
