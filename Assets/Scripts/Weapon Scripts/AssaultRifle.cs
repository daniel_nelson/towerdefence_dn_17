﻿using UnityEngine;
using System.Collections;

public class AssaultRifle : BaseWeapon {

	GameObject ch;

	bool isDown;

	public AudioClip assaultRifleReload;

	public AudioClip dryFire;

	float timeBetweenShots = 0;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateAmmo ();

		Debug.Log (currentAmmo + "/" + currentExtraAmmo);


		isDown = Input.GetMouseButton (0);

		if (isDown) {
			timeBetweenShots += Time.deltaTime;
			if (timeBetweenShots > 0.1f){
				gunAnim.Stop ();
				timeBetweenShots = 0;
			}
		}

		if (!gunAnim.isPlaying) {


			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if (isDown) {
				if (currentAmmo > 0) {
					AudioSource gunSound = GetComponent<AudioSource> ();
					if (isADS) {
						gunAnim.Play ("ADSFire");
					} else {
						gunAnim.Play ("Hipfire");
					}
					playerShoot = transform.root.GetComponent<PlayerShoot> ();
					print (playerShoot);
					playerShoot.currentWeapon = this;
					playerShoot.Shoot ();
					muzzleFlash.Play ();
					gunSound.Play ();
					currentAmmo--;
				} else {
					if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && currentAmmo <= 0){
						gunSound.PlayOneShot (dryFire);
					}
				}


			} 

			if (Input.GetKeyDown (KeyCode.R)) {
				if (isADS) {
					if (currentExtraAmmo > 0) {
						gunAnim.Play ("UnADS");
						gunAnim.PlayQueued ("Reload");
						gunAnim.PlayQueued ("ADS");
		
						gunSound.PlayOneShot (assaultRifleReload);
						if (currentExtraAmmo >= maxClipSize) {
							currentExtraAmmo -= maxClipSize - currentAmmo;
							currentAmmo = maxClipSize;	
						} else {
							currentAmmo += currentExtraAmmo;
							currentExtraAmmo = 0;
						}

					}
					
				} else {
					if (currentExtraAmmo > 0) {
						gunAnim.Play ("Reload");

						gunSound.PlayOneShot (assaultRifleReload);
						if (currentExtraAmmo >= maxClipSize) {
							currentExtraAmmo -= maxClipSize - currentAmmo;
							currentAmmo = maxClipSize;	
						} else {
							currentAmmo += currentExtraAmmo;
							currentExtraAmmo = 0;
						}

					}
				}

			}

		}

	}
		
}
