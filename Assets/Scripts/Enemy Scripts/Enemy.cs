﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class Enemy : BaseHealth {

    public const string PLACEHOLDER = "Placeholder";
	public const string FLYING = "Flying";
	public const string FASTFLYING = "FastFlying";

    [SerializeField]
    public string type;
	[SerializeField]//offsetToUpdatePath is how far the current target is away from the end of the current path in order to get a new path
	protected float weaponDamage, weaponRange, moveSpeed, offsetToUpdatePath; 
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected int enemyId;

	protected List<Vector2> path;
	protected Vector3 moveTarget;
	protected Vector3 targetOldPos;

	protected Pathfinder pathfinder;
	protected Transform playerBase;
	protected Transform currentTarget;
    protected Vector3 targetPos;

    public GameObject explosion;
	public AudioSource hummingSource;
	public AudioClip hummingSound;
	public AudioClip hummingDamageSound;
	public AudioSource damageSource;
	public AudioClip[] ricochetSounds;
	public AudioClip explosionSound;
    [SyncVar]
    Vector3 pos;
	public ParticleSystem sparks;
	public ParticleSystem damageSparks;

	public int fundsWorth;

	public bool isDead = false;

	public enum Target { PlayerBase, Player, Turret }
	private Target _target = Target.PlayerBase;
	public Target target {
		get { return _target; }
		set { _target = value; }
	}
    private float distanceToTarget;
	public virtual void Start() {
		pathfinder = new Pathfinder();
		playerBase = GameObject.Find ("EndZone").transform;
		UpdatePath ();
		Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());

		hummingSource.clip = hummingSound;
		hummingSource.Play ();
	}

	protected virtual void Update () {
        if (currentHealth <= maxHealth / 2) {
            if (hummingSource.clip.GetInstanceID() != hummingDamageSound.GetInstanceID()) {
                hummingSource.Stop();
                hummingSource.clip = hummingDamageSound;
                hummingSource.Play();
            }

            if (!damageSparks.isPlaying) {
                damageSparks.Play();
            }
        }

        if (isServer) { //only run movement stuff on server

            pos = transform.position;

            if (path.Count < 5 || Vector3.Distance(currentTarget.position, targetOldPos) > offsetToUpdatePath) {
                UpdatePath();
            }

            distanceToTarget = Vector3.Distance(currentTarget.position, transform.position);
            //target closest. player or base
            UpdateTarget();
            FollowPath();
        } else {
            transform.position = Vector3.Lerp(transform.position, pos, 1.1f * moveSpeed * Time.deltaTime);
        }

        targetOldPos = targetPos;
    }
    private void UpdateTarget() {
        Vector3 closestPlayer = GameManager.GetClosestPlayer(transform.position).transform.position;
        float distanceToPlayer = Vector3.Distance(closestPlayer, transform.position);
        float distanceToBase = Vector3.Distance(playerBase.position, transform.position);
        target = distanceToBase > distanceToPlayer ? Target.Player : Target.PlayerBase;
        targetPos = target == Target.Player ? closestPlayer : playerBase.position;
    }
	void OnTriggerEnter(Collider collider){
        if(collider.transform.name == "EndZone") {
            Die();
            return;
        }
		UpdatePath ();
	}
    void OnCollisionEnter(Collision collision) {
        foreach(ContactPoint contact in collision.contacts)
        if (contact.otherCollider.transform.name == "EndZone") {
            Die();
            return;
        }else if (contact.otherCollider.transform.name.Contains("Player")) {
            Die();
            return;
         }
        UpdatePath();
    }
    protected override void Die() {
        if (isServer) {
            Explode();

            Enemy enemy = default(Enemy);
            string ID = "";
            foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
                if (kp.Value.transform.position == transform.position) {
                    enemy = kp.Value;
                    ID = kp.Key;
                    break;
                }
            }
            if (enemy != default(Enemy)) {

                EnemyManager.enemies.Remove(ID);
                NetworkServer.Destroy(enemy.gameObject);
            }
        }
    }
    protected virtual void Explode() {
        //damage playerbase if in range
        if (Vector3.Distance(playerBase.position, transform.position) < weaponRange) {
            //	if (!GetComponentInChildren<ParticleSystem> ().isPlaying) {
            playerBase.GetComponent<Base>().TakeDamage(weaponDamage);
            Debug.Log("Enemy hit base for: " + weaponDamage);
            // }
        }
        //damage players in range
        List<Player> playersInRange = GameManager.GetPlayersInRange(transform.position, weaponRange);
        if (playersInRange.Count > 0) {
            foreach (Player player in playersInRange) {
                player.TakeDamage(weaponDamage);

            }
        }
        // RpcExplode(transform.position);
        GameObject exp = (GameObject)GameObject.Instantiate(explosion, transform.position, new Quaternion());
        NetworkServer.Spawn(exp);
        isDead = true;
    }
    protected void FollowPath(){
		if(path != null && currentPathIndex < path.Count){
			if (!CheckPathClear ()) {//TODO: UPDATE THIS TO ONLY RUN SOMETIMES TO IMPROVE PERFORMANCE
				UpdatePath ();
			}
			if(moveTarget != transform.position){
				CalculateMoveTarget();
			}
			else{
				currentPathIndex++;
				CalculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	protected void CalculateMoveTarget(){
		moveTarget = Grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
	}
	protected bool CheckPathClear(){
		for (int i = 0; currentPathIndex + i < path.Count && i < pathAheadCheck; i++) {
			if(!pathfinder.checkWalkable((int)path[currentPathIndex + i].x, (int)path[currentPathIndex + i].y)){
				return false;
			}
		}
		return true;
	}
	public void UpdatePath(){
		switch(target){
		case Target.Player:
			UpdatePath (GameManager.GetClosestPlayer(transform.position).gameObject.transform);
			break;
		case Target.PlayerBase:
			UpdatePath (playerBase);
			break;
		}
	}	
	protected void UpdatePath(Transform _newTarget){
		currentTarget = _newTarget;
        targetPos = _newTarget.position;
		try{
			path = pathfinder.findPath (Grid.GetVector2 (transform.position), Grid.GetVector2 (_newTarget.position));
		}catch(Exception e){
			Debug.LogError ("The error below is expected. Doesn't seem to affect anything and I decided to move on after trying to fix for a while. If you want to attemp to fix it, first delete this try catch as it moves the error. - Riordan");

		}
		currentPathIndex = 0;
	}
    public override void TakeDamage(float _dmg) {
        sparks.Play();

        //For playing sounds only.
        int i = UnityEngine.Random.Range(0, ricochetSounds.Length);

        AudioClip clip = ricochetSounds[i];
        damageSource.Stop();
        damageSource.clip = clip;
        damageSource.Play();

        base.TakeDamage(_dmg);
    }
}
