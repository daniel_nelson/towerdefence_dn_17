﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

/*
 * Maily keeps track of the enemies - and also match settings
 * Since there is only one Game manger instance per scene, this has been made as a singleton
 * 
*/
public class EnemyManager : NetworkBehaviour {

	public static EnemyManager singleton; 
    
	void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Enemy manager is running"); 
		} else {
			singleton = this; 
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}

	/// <summary>
	/// region are easy to keep track of sections of code - the first region here is for
	/// the enemy code
	/// </summary>
	#region enemy tracking 
	private const string ENEMY_ID_PREFIX = "Enemy "; 

	public static Dictionary<string, Enemy> enemies = new Dictionary<string, Enemy>();
    public static Dictionary<string, Enemy> spawnableEnemies = new Dictionary<string, Enemy>();
    public List<Enemy> spawnableEnemyList = new List<Enemy>();

    public override void OnStartClient() {
        base.OnStartClient();

        for (int i = 0; i < spawnableEnemyList.Count; i++) {
			string type = spawnableEnemyList [i].type;
			Enemy enemy = spawnableEnemyList[i];
            spawnableEnemies.Add(spawnableEnemyList[i].type, spawnableEnemyList[i]);
        }
    }
    //you can get a enemy from this getter
    public static Enemy GetEnemy(string _enemyID){
        if(enemies.ContainsKey(_enemyID))
		    return enemies[_enemyID];
        print("Couldn't find enemy with ID: " + _enemyID);
        return default(Enemy);
	}

	//Find the closest enemy to position
	public static Enemy GetClosestEnemy(Vector3 position){
		Enemy currentClosest = default(Enemy);
		float currentClosestDistance = 0f;
		foreach (Enemy enemy in enemies.Values) {
			float distance = Vector3.Distance (enemy.gameObject.transform.position, position);
			if(distance < currentClosestDistance || currentClosestDistance == 0f){
				currentClosest = enemy;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Enemy)) {
			return currentClosest;
		}
		Debug.LogError ("No enemy found. Returned default enemy likely breaking script using this method");
		return default(Enemy);
	}
    #endregion


    Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range) {
        Vector3 spawnPos;
        spawnPos.x = centreSpawnPos.x + (Random.Range(-range, range));
        spawnPos.z = centreSpawnPos.z + (Random.Range(-range, range));
        spawnPos.y = centreSpawnPos.y;
        return spawnPos;
    }
    

    #region Enemy Functions
    [Command]
    public void CmdSpawnEnemy(string _type, Vector3 _pos, float _radius) {
        Enemy enemy = EnemyManager.spawnableEnemies[_type];

        Vector3 spawnPos = UpdateSpawnPos(_pos, _radius);
        GameObject obj = enemy.gameObject;
        obj = (GameObject)Instantiate(obj, spawnPos, Quaternion.identity);

        NetworkServer.Spawn(obj);
        string ID = obj.GetComponent<NetworkIdentity>().netId.ToString();
        obj.name = _type + " " + ID;
        enemy = obj.GetComponent<Enemy>();
        EnemyManager.enemies.Add(obj.name, enemy);
    }

    [Command]
    public void CmdDestroyEnemy(Vector3 _pos) {
        Enemy enemy = default(Enemy);
        string ID = "";
        foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
            if (kp.Value.transform.position == _pos) {
                enemy = kp.Value;
                ID = kp.Key;
                break;
            }
        }
        if (enemy != default(Enemy)) {
            EnemyManager.enemies.Remove(ID);
            NetworkServer.Destroy(enemy.gameObject);
        }
    }
    #endregion
}
