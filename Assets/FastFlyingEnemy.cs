﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FastFlyingEnemy : Enemy {

	public Rigidbody body;
	public ParticleSystem smoke;
    private float startHeight;
	// Use this for initialization
	public override void Start () {
		type = "FastFlying";
		fundsWorth = 100;
		currentHealth = maxHealth;
        startHeight = transform.position.y;
		base.Start ();
	}

	protected override void Update () {

        base.Update();
        if (!smoke.gameObject.activeSelf) return;
        if (currentHealth <= maxHealth / 2) {
            //if (!smoke.isPlaying) {
            smoke.Play();
            //}
        } else
        {
			smoke.Stop ();
		}
        
    }

	
}


